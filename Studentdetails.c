#include <stdio.h>
struct student {
    char firstName[50];
    int roll;
    char subject[50];
    float marks;
} s[10];

int main() {
    int i;
    printf("Enter information of students:\n");

    for (i = 0; i < 5; ++i) {
        s[i].roll = i + 1;
        printf("\n number%d,\n", s[i].roll);
        printf("Enter first name: ");
        scanf("%s", s[i].firstName);
        printf("Enter the subject %d:", i);
        scanf("%s", &s[i].subject);
        printf("Enter marks: ");
        scanf("%f", &s[i].marks);
    }
    printf("Displaying Information:\n\n");

    for (i = 0; i < 5; ++i) {
        printf("\n number: %d\n", i + 1);
        printf("First name: ");
        puts(s[i].firstName);
        printf("Subject: %s\n", s[i].subject);
        printf("Marks: %.1f", s[i].marks);
        printf("\n");
    }
    return 0;
}
